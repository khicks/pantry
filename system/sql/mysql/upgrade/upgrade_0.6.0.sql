ALTER TABLE `recipes`
    ADD `serving_size` varchar(40) NOT NULL AFTER `cook_time`,
    ADD `calories` mediumint(6) NOT NULL AFTER `serving_size`;
