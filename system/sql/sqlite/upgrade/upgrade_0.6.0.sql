ALTER TABLE "recipes" ADD COLUMN "serving_size" varchar(40) default "" NOT NULL;
ALTER TABLE "recipes" ADD COLUMN "calories" mediumint(6) default 0 NOT NULL;
